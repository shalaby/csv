##Data Base

In order to use this app you have to install and secure CouchDB server and update environment files (src/environments) with it's credentials.

##CSV File

There is an example CSV file in CSV folder and I named it's header cells like this :
Serial, Name, Email, Phone, Image, Title.