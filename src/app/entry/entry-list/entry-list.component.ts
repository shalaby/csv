import {Component, OnInit, NgZone} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Entry} from '../shared/entry.model';
import {EntryService} from '../shared/entry.service';

@Component({
  selector   : 'app-entry-list',
  templateUrl: './entry-list.component.html'
})
export class EntryListComponent implements OnInit {
  showAddEntry = false;
  entry: Entry;
  entries: Entry[];
  rows = 50;
  notifications: any;
  index: number;
  loading = false;
  id: string;
  name: string;

  constructor(private entryService: EntryService,
              private zone: NgZone,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    this.route.queryParams.subscribe(params => {
      this.name = params['name'];
    });
    this.getEntries();
    this.entryService.getChangeListener()
        .subscribe(() => {
          this.getEntries();
        });
  }

  async getEntries() {
    const entries = await this.entryService.findByCsvId(this.id);
    this.zone.run(() => {
      this.entries = entries;
    });
  }

  editEntry(entry) {
    this.entry = entry;
    // this.showEditEntry = true;
  }

  async deleteEntry(id) {
    await this.entryService.delete(id);
  }

  closeDialog(event) {
    this.showAddEntry = false;
    // this.showEditEntry = false;
  }

  displayErrorMessage(error) {
  }
}
