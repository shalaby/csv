import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EntryListComponent} from '../entry-list/entry-list.component';

const EntryRoutes: Routes = [
  {
    path    : 'csv/:id',
    children: [
      {path: '', component: EntryListComponent}
    ]
  }
];

export const EntryRouting: ModuleWithProviders = RouterModule.forChild(EntryRoutes);
