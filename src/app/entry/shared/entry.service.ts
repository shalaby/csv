import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {DbService} from '../../shared/db.service';
import {Entry} from './entry.model';
import 'rxjs/add/operator/first';

@Injectable()
export class EntryService {
  db: any;
  private listener: EventEmitter<any> = new EventEmitter();
  headers: HttpHeaders;
  serverUrl = environment.webServer.url + 'upload';
  username = environment.webServer.username;
  password = environment.webServer.password;

  constructor(private dbService: DbService,
              private http: HttpClient) {
    this.db = this.dbService.db;
    this.dbService.getChangeListener().subscribe(change => {
      this.listener.emit(change);
    });
    this.headers = new HttpHeaders()
        .set('Authorization', 'Basic ' + btoa(this.username + ':' + this.password));
  }

  getAll() {
    return this.db.allDocs({
      include_docs: true,
      startkey: 'entry',
      endkey: 'entry\uffff'
    });
  }

  async save(doc: Entry, rows) {
    doc._id = 'entry_' + this.dbService.generateId();
    return this.db.put(doc);
  }

  async update(document: Entry) {
    const oldDocument = await this.findOne(document._id);
    const newDocument = Object.assign(oldDocument, document);
    return this.db.put(newDocument);
  }

  async findOne(id: string) {
      return await this.db.get(id);
  }

  async delete(id: string) {
    const doc = await this.findOne(id);
    return this.db.remove(doc);
  }

  async getAllReduced() {
    const rawDocs = await this.getAll();
    return rawDocs.rows.reduce((acc, value, index) => {
      acc[index] = value.doc;
      return acc;
    }, []);
  }

  async findByCsvId(id) {
    const docs = await this.getAllReduced();
    return docs.filter(doc => (doc.csvId === id));
  }

  public getChangeListener() {
    return this.listener;
  }
}
