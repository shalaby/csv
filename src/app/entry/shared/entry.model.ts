export interface Entry {
  _id?: string;
  Serial?: number;
  Name?: string;
  Email?: string;
  Phone?: number;
  Image?: string;
  Title?: string;
  status?: string;
}
