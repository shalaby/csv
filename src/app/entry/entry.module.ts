import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {EntryListComponent} from './entry-list/entry-list.component';
import {EntryRouting} from './shared/entry.routing';
import {EntryService} from './shared/entry.service';

@NgModule({
  imports: [
    SharedModule,
    EntryRouting
  ],
  declarations: [
    EntryListComponent
  ],
  providers: [
    EntryService
  ]
})
export class EntryModule {
}
