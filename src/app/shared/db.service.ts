import { Injectable, EventEmitter } from '@angular/core';
import 'rxjs/add/operator/map';
import PouchDB from 'pouchdb';
import { ulid } from 'ulid';
import {environment} from '../../environments/environment';

@Injectable()
export class DbService {
  private isInstantiated: boolean;
  public db: any;
  private listener: EventEmitter<any> = new EventEmitter();

  public constructor() {
    if (!this.isInstantiated) {
      this.db = new PouchDB('avocato');
      this.db.changes({
        live: true,
        since: 'now'
      }).on('change', change => {
        this.listener.emit(change);
      });
      this.isInstantiated = true;
    }
    this.sync();
  }

  public sync() {
    const remoteDB = environment.DbServer.url + 'csv';
    const remoteDatabase = new PouchDB(remoteDB, {
      skip_setup: true,
      ajax: {
        headers: {
          Authorization: 'Basic ' + window.btoa(environment.DbServer.username + ':' + environment.DbServer.password)
        }
      }
    });
    this.db.sync(remoteDatabase, {
      live: true,
      retry: true
    }).on('change', function (info) {
      // console.log(info);
    }).on('paused', function (err) {
      // replication paused (e.g. replication up to date, user went offline)
    }).on('active', function () {
      // replicate resumed (e.g. new changes replicating, user went back online)
    }).on('denied', function (err) {
      console.log(err);
    }).on('complete', function (info) {
      // console.log(info);
    }).on('error', function (err) {
      console.log(err);
    });
  }

  public getChangeListener() {
    return this.listener;
  }

  public generateId() {
    return ulid();
  }

}
