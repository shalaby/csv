import {Component, ViewChild, Input, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PapaParseService} from 'ngx-papaparse';
import {Csv} from '../shared/csv.model';
import {CsvService} from '../shared/csv.service';

@Component({
  selector   : 'app-csv-add',
  templateUrl: './csv-add.component.html'
})
export class CsvAddComponent {

  csv: Csv;
  @ViewChild('file') file;
  @Input() show = false;
  @Output() hide = new EventEmitter();
  csvForm: FormGroup;
  notifications: any;
  fileInfo: any;

  constructor(private csvService: CsvService,
              private formBuilder: FormBuilder,
              private papa: PapaParseService) {
    this.csvForm = this.formBuilder.group({
      name: ['', [Validators.required]]
    });
  }

  async save(csvForm) {
    const csv = csvForm.value;
    csv.date = new Date().toISOString().substring(0, 10);
    csv.completed = 0;
    const fileInput = this.file.nativeElement;
    const file = fileInput.files[0];
    this.papa.parse(file, {
      header: true,
      complete: async (results) => {
        const rows = results.data;
        csv.count = rows.length;
        csv.incomplete = rows.length;
          try {
            this.closeDialog();
            await this.csvService.save(csv, rows);
          } catch (e) {
            console.log(e);
            this.displayErrorMessage();
          }
      }
    });
  }

  upload() {
    const file = this.file.nativeElement;
    if (file.files && file.files[0]) {
      console.log(file.files[0]);
      this.fileInfo = file.files[0];
      const formData = new FormData();
      formData.append('file', file.files[0]);
      return this.csvService.upload(formData);
    }
  }

  fileSelected() {
    const file = this.file.nativeElement;
    if (file.files && file.files[0]) {
      this.fileInfo = file.files[0];
    }
  }

  closeDialog() {
    // this.csvForm.reset();
    this.hide.emit(true);
  }

  displayErrorMessage() {
  }


}
