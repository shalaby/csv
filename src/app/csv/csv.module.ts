import {NgModule} from '@angular/core';
import {PapaParseModule} from 'ngx-papaparse';
import {SharedModule} from '../shared/shared.module';
import {CsvAddComponent} from './csv-add/csv-add.component';
import {CsvListComponent} from './csv-list/csv-list.component';
import {CsvRouting} from './shared/csv.routing';
import {CsvService} from './shared/csv.service';

@NgModule({
  imports: [
    PapaParseModule,
    CsvRouting,
    SharedModule
  ],
  declarations: [
    CsvListComponent,
    CsvAddComponent
  ],
  providers: [CsvService]
})
export class CsvModule {
}
