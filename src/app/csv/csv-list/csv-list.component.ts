import {Component, OnInit, NgZone} from '@angular/core';
import {Router} from '@angular/router';
import {Csv} from '../shared/csv.model';
import {CsvService} from '../shared/csv.service';

@Component({
  selector   : 'app-csv-list',
  templateUrl: './csv-list.component.html'
})
export class CsvListComponent implements OnInit {
  showAddCsv = false;
  csv: Csv;
  csvs: Csv[];
  rows = 50;
  notifications: any;
  index: number;
  loading = false;

  constructor(private csvService: CsvService,
              private zone: NgZone,
              private router: Router) {
  }

  ngOnInit() {
    this.getCsvs();
    this.csvService.getChangeListener()
        .subscribe(() => {
          this.getCsvs();
        });
  }

  async getCsvs() {
    const csvs = await this.csvService.getAllReduced();
    this.zone.run(() => {
      this.csvs = csvs;
    });
  }

  showCsvDetails(id, name) {
    this.router.navigate(['/csv', id], { queryParams: { name } });
  }

  editCsv(csv) {
    this.csv = csv;
    // this.showEditCsv = true;
  }

  async deleteCsv(id) {
    await this.csvService.delete(id);
  }

  closeDialog(event) {
    this.showAddCsv = false;
    // this.showEditCsv = false;
  }

  displayErrorMessage(error) {
  }
}
