import {HttpClient, HttpHeaders} from '@angular/common/http';
import {EventEmitter, Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {DbService} from '../../shared/db.service';
import {Csv} from './csv.model';
import 'rxjs/add/operator/first';

@Injectable()
export class CsvService {
  db: any;
  private listener: EventEmitter<any> = new EventEmitter();
  headers: HttpHeaders;
  serverUrl = environment.webServer.url + 'upload';
  username = environment.webServer.username;
  password = environment.webServer.password;

  constructor(private dbService: DbService,
              private http: HttpClient) {
    this.db = this.dbService.db;
    this.dbService.getChangeListener().subscribe(change => {
      this.listener.emit(change);
    });
    this.headers = new HttpHeaders()
        .set('Authorization', 'Basic ' + btoa(this.username + ':' + this.password));
  }

  getAll() {
    return this.db.allDocs({
      include_docs: true,
      startkey: 'csv',
      endkey: 'csv\uffff'
    });
  }

  async save(doc: Csv, rows) {
    doc._id = 'csv_' + this.dbService.generateId();
    await this.db.put(doc);
    let errors = 0;
    rows.forEach(async row => {
      row._id = 'entry_' + this.dbService.generateId();
      row.csvId = doc._id;
      const fileName = await this.upload(row.Image).first().toPromise();
      if (typeof fileName === 'string') {
        row.fileName = fileName;
        row.status = 'Uploaded';
      } else {
        errors++;
        row.status = 'Error';
      }
      await this.db.put(row);
    });
    const updatedDoc: any = {
      _id: doc._id
    };
    updatedDoc.incomplete = errors;
    updatedDoc.completed = doc.count - errors;
    return this.update(updatedDoc);
  }

  async update(document: Csv) {
    const oldDocument = await this.findOne(document._id);
    const newDocument = Object.assign(oldDocument, document);
    return this.db.put(newDocument);
  }

  async findOne(id: string) {
      return await this.db.get(id);
  }

  async delete(id: string) {
    const doc = await this.findOne(id);
    return this.db.remove(doc);
  }

  async getAllReduced() {
    const rawDocs = await this.getAll();
    return rawDocs.rows.reduce((acc, value, index) => {
      acc[index] = value.doc;
      return acc;
    }, []);
  }

  upload(link) {
    return this.http.post(this.serverUrl, {link}, {responseType: 'text'});
  }

  public getChangeListener() {
    return this.listener;
  }
}
