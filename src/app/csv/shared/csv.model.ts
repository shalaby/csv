export interface Csv {
  _id?: string;
  date?: Date | string;
  name?: string;
  count?: number;
  completed?: number;
  incomplete?: number;
}
