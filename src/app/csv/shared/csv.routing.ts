import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CsvListComponent} from '../csv-list/csv-list.component';

const CsvRoutes: Routes = [
  {
    path    : 'csv',
    children: [
      {path: '', component: CsvListComponent}
    ]
  }
];

export const CsvRouting: ModuleWithProviders = RouterModule.forChild(CsvRoutes);
