import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppRoutingModule} from './app-routing.module';

import {AppComponent} from './app.component';
import {CsvModule} from './csv/csv.module';
import {EntryModule} from './entry/entry.module';
import {SharedModule} from './shared/shared.module';
import {UiModule} from './ui/ui.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    UiModule,
    CsvModule,
    EntryModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
