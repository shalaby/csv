import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {LayoutComponent} from './layout/layout.component';
import {HeaderComponent} from './layout/header/header.component';
import {MainComponent} from './layout/main/main.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports     : [
    RouterModule,
    SharedModule
  ],
  declarations: [LayoutComponent, HeaderComponent, MainComponent],
  exports     : [LayoutComponent]
})
export class UiModule {
}
