// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  DbServer  : {
    url     : 'http://localhost:5984/',
    username: 'admin',
    password: 'adminadmin'
  },
  webServer : {
    url     : 'http://localhost:3000/',
    username: 'admin',
    password: '$2y$10$i2Kqf5gyTnRkKxde3l7HBuy8FodZDSB8XJ.xS4T71PC8dYa/JeE.K'
  }
};
