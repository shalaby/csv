export const environment = {
  production: true,
  DbServer  : {
    url     : 'http://localhost:5984/',
    username: 'admin',
    password: 'adminadmin'
  },
  webServer : {
    url     : 'http://localhost:3000/',
    username: 'admin',
    password: '$2y$10$i2Kqf5gyTnRkKxde3l7HBuy8FodZDSB8XJ.xS4T71PC8dYa/JeE.K'
  }
};
